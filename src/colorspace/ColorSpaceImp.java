package colorspace;

import java.awt.Image;
import java.awt.color.ICC_ColorSpace;
import java.awt.color.ICC_Profile;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.awt.Color;

public class ColorSpaceImp implements ColorSpace {
	@Override
	public float[] rgbTohsv(int rgb) {
		Color color = new Color(rgb);
		double r = color.getRed() / 255.0;
		double g = color.getGreen() / 255.0;
		double b = color.getBlue() / 255.0;

		// h, s, v = hue, saturation, value
		double min = Math.min(Math.min(r, g), b);
		double max = Math.max(Math.max(r, g), b);
		double delta = max - min;
		Double hue = max;
		Double sat = max;
		Double val = max;
		if (delta == 0) {
			hue = 0.0;
			sat = 0.0;
		} else {

			sat = delta / max;

			double delR = (((max - r) / 6) + (delta / 2)) / delta;
			double delG = (((max - g) / 6) + (delta / 2)) / delta;
			double delB = (((max - b) / 6) + (delta / 2)) / delta;

			if (r == max) {
				hue = delB - delG;
			} else if (g == max) {
				hue = (1 / 3) + delR - delB;
			} else if (b == max) {
				hue = (2 / 3) + delG - delR;
			}

			if (hue < 0)
				hue += 1;
			if (hue > 1)
				hue -= 1;
		}
		return new float[] { hue.floatValue(), sat.floatValue(), val.floatValue() };
	}

	@Override
	public int[] rgbTocmyk(int rgb){
		Color color = new Color(rgb);
		double percentageR = color.getRed() / 255.0 * 100;
        double percentageG = color.getGreen() / 255.0 * 100;
        double percentageB = color.getBlue() / 255.0 * 100;

        double k = 100 - Math.max(Math.max(percentageR, percentageG), percentageB);

        if (k == 100) {
            return new int[]{ 0, 0, 0, 100 };
        }

        int c = (int)((100 - percentageR - k) / (100 - k) * 100);
        int m = (int)((100 - percentageG - k) / (100 - k) * 100);
        int y = (int)((100 - percentageB - k) / (100 - k) * 100);

        return new int[]{ c, m, y, (int)k };

	}

	@Override
	public BufferedImage rgbToyuv(BufferedImage original) {
		return processImage(original);
	}

	private BufferedImage processImage(BufferedImage original) {
		int width = original.getWidth();
		int height = original.getHeight();

		for (int w = 0; w < width; w++) {
			for (int h = 0; h < height; h++) {
				int rgb = original.getRGB(w, h);
				int alpha = (rgb >> 24) & 0xff;
				int red = (rgb >> 16) & 0xff;
				int green = (rgb >> 8) & 0xff;
				int blue = rgb & 0xff;

				double Y = 0.299 * red + 0.587 * green + 0.114 * blue;
				double U = -0.169 * red - 0.331 * green + 0.500 * blue + 128;
				double V = 0.500 * red - 0.419 * green - 0.081 * blue + 128;
				if (Y < 0)
					Y = 0;
				else if (Y > 255)
					Y = 255;
				if (U < 0)
					U = 0;
				else if (U > 255)
					U = 255;
				if (V < 0)
					V = 0;
				else if (V > 255)
					V = 255;
				red = (int) Y;
				green = (int) U;
				blue = (int) V;
				rgb = (alpha << 24) | (red << 16) | (green << 8) | blue;
				original.setRGB(w, h, rgb);
			}
		}
		return original;

	}
}

enum SPACE {
	HSV, CMYK, YUV
}
