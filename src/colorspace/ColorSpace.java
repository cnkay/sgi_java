package colorspace;

import java.awt.image.BufferedImage;
import java.io.IOException;

public interface ColorSpace {
	float[] rgbTohsv(int rgb);
	int[] rgbTocmyk(int rgb);
	BufferedImage rgbToyuv(BufferedImage original);
}
