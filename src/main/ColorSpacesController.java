package main;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

import javax.imageio.ImageIO;

import colorspace.ColorSpace;
import colorspace.ColorSpaceImp;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class ColorSpacesController {

	private BufferedImage original;
	private BufferedImage originalSize;
	private ColorSpace colorSpace;
	private DecimalFormat df;
	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private ImageView originalView;

	@FXML
	private TextField txtRatio;

	@FXML
	private RadioButton radioYuv;

	@FXML
	private ImageView hsvView;

	@FXML
	private ImageView cmykView;

	@FXML
	private ImageView yuvView;

	@FXML
	private Label lblRGB;

	@FXML
	private Label lblHSV;

	@FXML
	private Label lblRGBc;

	@FXML
	private Label lblCmyk;

	@FXML
	private Button btnSave;

	@FXML
	void btnSaveOnClick(ActionEvent event) throws IOException {
		if (radioYuv.isSelected()) {

			BufferedImage img = colorSpace.rgbToyuv(cloneBufferedImage(originalSize));
			String filename = "output_yuv";
			try {
				File file = new File(filename);
				ImageIO.write(img, "png", file);
				Alert alert = new Alert(AlertType.INFORMATION,
						"Resim dosyanız " + file.getAbsolutePath() + " yoluna kaydedildi!", ButtonType.OK);
				alert.show();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@FXML
	void hsvViewOnClick(MouseEvent event) {
		// double[] values = colorSpace.rgbTohsv(original);

	}

	@FXML
	void cmykViewOnMouseMove(MouseEvent event) {
		Robot robot;
		try {
			robot = new Robot();
			Color color = robot.getPixelColor((int) event.getScreenX(), (int) event.getScreenY());
			int[] cmykValues = colorSpace.rgbTocmyk(color.getRGB());

			lblRGBc.setText(color.getRed() + ", " + color.getBlue() + ", " + color.getGreen());
			lblCmyk.setText(cmykValues[0] + ", " + cmykValues[1] + ", " + cmykValues[2] + ", " + cmykValues[3]);
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	void hsvViewOnMouseMove(MouseEvent event) {
		Robot robot;
		try {
			robot = new Robot();
			Color color = robot.getPixelColor((int) event.getScreenX(), (int) event.getScreenY());
			float[] hsvValues = colorSpace.rgbTohsv(color.getRGB());

			lblRGB.setText(color.getRed() + ", " + color.getBlue() + ", " + color.getGreen());
			lblHSV.setText(df.format(hsvValues[0]) + ", " + df.format(hsvValues[1]) + ", " + df.format(hsvValues[2]));
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	void initialize() {

		assert originalView != null : "fx:id=\"originalView\" was not injected: check your FXML file 'ColorSpaces.fxml'.";
		assert txtRatio != null : "fx:id=\"txtRatio\" was not injected: check your FXML file 'ColorSpaces.fxml'.";
		assert radioYuv != null : "fx:id=\"radioYuv\" was not injected: check your FXML file 'ColorSpaces.fxml'.";
		assert hsvView != null : "fx:id=\"hsvView\" was not injected: check your FXML file 'ColorSpaces.fxml'.";
		assert cmykView != null : "fx:id=\"cmykView\" was not injected: check your FXML file 'ColorSpaces.fxml'.";
		assert yuvView != null : "fx:id=\"yuvView\" was not injected: check your FXML file 'ColorSpaces.fxml'.";
		assert btnSave != null : "fx:id=\"btnSave\" wpaceas not injected: check your FXML file 'ColorSpaces.fxml'.";
		assert lblRGB != null : "fx:id=\"lblRGB\" was not injected: check your FXML file 'ColorSpaces.fxml'.";
		assert lblHSV != null : "fx:id=\"lblHSV\" was not injected: check your FXML file 'ColorSpaces.fxml'.";
		assert lblRGBc != null : "fx:id=\"lblRGBc\" was not injected: check your FXML file 'ColorSpaces.fxml'.";
		assert lblCmyk != null : "fx:id=\"lblCmyk\" was not injected: check your FXML file 'ColorSpaces.fxml'.";
		df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		radioYuv.setSelected(true);
	}

	private Dimension getScaledDimension(Dimension imageSize, Dimension boundary) {

		double widthRatio = boundary.getWidth() / imageSize.getWidth(); // For keep aspect ratio
		double heightRatio = boundary.getHeight() / imageSize.getHeight();
		double ratio = Math.min(widthRatio, heightRatio);
		txtRatio.setText(ratio + "");
		return new Dimension((int) (imageSize.width * ratio), (int) (imageSize.height * ratio));
	}

	private void setImageToImageView(BufferedImage img, ImageView view) throws IOException {
		Dimension dimensionOfImageView = new Dimension(ImageViewDimension.WIDTH.value(),
				ImageViewDimension.HEIGHT.value()); // ImageView's width,height
		Dimension dimensionOfImageFile = new Dimension(img.getWidth(), img.getHeight());
		Dimension newDimension = getScaledDimension(dimensionOfImageFile, dimensionOfImageView);
		BufferedImage newImg = resize(img, newDimension.height, newDimension.width);
		original = cloneBufferedImage(newImg);
		originalView.setImage(SwingFXUtils.toFXImage(newImg, null));
		setAnotherImageViews();
	}

	private void setAnotherImageViews() throws IOException {
		colorSpace = new ColorSpaceImp();
		BufferedImage yuvBufferedImage = colorSpace.rgbToyuv(cloneBufferedImage(original));
		BufferedImage cmykBufferedImage = cloneBufferedImage(original);
		BufferedImage hsvBufferedImage = cloneBufferedImage(original);
		hsvView.setImage(SwingFXUtils.toFXImage(hsvBufferedImage, null));
		yuvView.setImage(SwingFXUtils.toFXImage(yuvBufferedImage, null));
		cmykView.setImage(SwingFXUtils.toFXImage(cmykBufferedImage, null));
	}

	public void setImagePath(String absolutePath) {
		try {
			BufferedImage img = ImageIO.read(new File(absolutePath));
			originalSize = cloneBufferedImage(img);
			setImageToImageView(img, originalView);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static BufferedImage cloneBufferedImage(BufferedImage img) {
		ColorModel colorModel = img.getColorModel();
		boolean isAlphaPremultiplied = colorModel.isAlphaPremultiplied();
		WritableRaster raster = img.copyData(null);
		return new BufferedImage(colorModel, raster, isAlphaPremultiplied, null);
	}

	private static BufferedImage resize(BufferedImage img, int height, int width) {
		Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH); // Resizing image
		BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = resized.createGraphics();
		g2d.drawImage(tmp, 0, 0, null);
		g2d.dispose();
		return resized;
	}
}

enum ImageViewDimension {
	WIDTH(200), HEIGHT(150);

	private final int dimensionValue;

	ImageViewDimension(int dimensionValue) {
		this.dimensionValue = dimensionValue;
	}

	public int value() {
		return this.dimensionValue;
	}
}
