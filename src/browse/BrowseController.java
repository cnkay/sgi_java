package browse;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javax.imageio.ImageIO;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import main.ColorSpacesController;
import javafx.stage.Stage;

public class BrowseController {
	private FileChooser chooser;
	@FXML
	private ResourceBundle resources;

	@FXML
	private AnchorPane paneAnchor;

	@FXML
	private URL location;

	@FXML
	private Button btnBrowse;

	@FXML
	private Label txtBrowse;

	@FXML
	void btnBrowseOnClick(MouseEvent event) {
		File selectedFile = chooser.showOpenDialog((Stage) paneAnchor.getScene().getWindow());
		if (selectedFile != null) {
			System.out.println("Selected file name : " + selectedFile.getName());
			System.out.println("Selected file absolute path : " + selectedFile.getAbsolutePath());
			String absolutePath = selectedFile.getAbsolutePath();
			System.out.println("Selected file path : " + selectedFile.getPath());
			try {
				if (ImageIO.read(selectedFile) == null)
					throw new IOException();
				FXMLLoader loader = new FXMLLoader(getClass().getResource("../main/ColorSpaces.fxml"));
				Parent root = (Parent) loader.load();
				ColorSpacesController controller = loader.getController();
				controller.setImagePath(absolutePath);
				Scene scene = new Scene(root, 800, 625);
				scene.getStylesheets().add(getClass().getResource("/main/colorspaces.css").toExternalForm());
				Stage stage = new Stage();
				stage.setTitle("Color Spaces");
				stage.setScene(scene);
				stage.setResizable(false);
				stage.show();
			} catch (IOException e) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Select an image file");
				alert.setHeaderText("Invalid image file");
				alert.setContentText("Image file is invalid or has not supported type!");
				alert.showAndWait();
				e.printStackTrace();
			}

		}
	}

	@FXML
	void initialize() {
		assert btnBrowse != null : "fx:id=\"btnBrowse\" was not injected: check your FXML file 'Main.fxml'.";
		assert txtBrowse != null : "fx:id=\"txtBrowse\" was not injected: check your FXML file 'Main.fxml'.";
		chooser = new FileChooser();
		chooser.setTitle("Select an image file");
		chooser.getExtensionFilters().addAll(new ExtensionFilter("PNG files(*.png)", "*.png"),
				new ExtensionFilter("JPEG files(*.jpg)", "*.jpg"));
	}
}